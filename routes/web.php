<?php

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\FrontController;
use App\Http\Controllers\ContactsController;
use App\Http\Controllers\BreweriesController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [FrontController::class, 'index'])->name('index');
Route::get('/breweries',[FrontController::class, 'breweries'])->name('breweries');
Route::get('/about', [FrontController::class, 'about'])->name('about');
Route::get('/team', [FrontController::class, 'team'])->name('team');
Route::get('/search', [FrontController::class, 'search'])->name('search');

// Contacts received
Route::post('/contacts/submit', [ContactsController::class, 'submit'])->name('contacts.submit');
Route::get('/contacts/thankyou', [ContactsController::class, 'thankyou'])->name('contacts.thankyou');

//Breweries
Route::post('/breweries/notify', [BreweriesController::class, 'notify'])->name('breweries.notify');
Route::get('/breweries/notify/thankyou', [BreweriesController::class, 'thankyou'])->name('breweries.thankyou');
Route::post('/breweries/{id}/approved', [BreweriesController::class, 'breweryApproved'])->name('brewery.approved');
Route::get('/breweries/{id}/details', [BreweriesController::class, 'details'])->name('breweries.details');
Route::post('/breweries/{id}/update', [BreweriesController::class, 'update'])->name('breweries.update');
Route::delete('/breweries/{id}/delete', [BreweriesController::class, 'delete'])->name('breweries.delete');
Route::post('/breweries/{id}/comments', [BreweriesController::class, 'addComment'])->name('breweries.comments.add');

//Beers
Route::post('/breweries/{id}/beers/sync', [BreweriesController::class, 'beersSync'])->name('breweries.beers.sync');




Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
