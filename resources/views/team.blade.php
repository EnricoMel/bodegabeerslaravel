@extends('layouts.app')

@section('content') 

<style>
    
    .masthead {
        background: linear-gradient(90deg, rgba(220, 178, 78, 0.6) 0%, rgba(220, 178, 178, 0.6) 100%), url("/img/team_bg.jpg");
    }      
    
    .bg-trasparency {
        background: linear-gradient(90deg, rgba(255, 255, 255, 0.6) 0%, rgba(255, 255, 255, 0.6) 100%);
    }
    
</style>

<!-- Header-->
<header class="masthead d-flex align-items-center">
    <div class="container px-4 px-lg-5 text-center bg-trasparency">
        <h1 class="mb-1">Ecco il nostro fantastico team</h1>
        
        <div class="container mt-5">
            <div class="row justify-content-center">
                @foreach ($members as $member)
                <div class="col-md-2">
                    <strong> {{ $member['name'] }} </strong>
                    <img src="{{ $member['img'] }}" alt="{{ $member['name'] }}" class="img-fluid">
                    <i> {{ $member['role'] }} </i>
                </div>                
                @endforeach 
            </div>
        </div>
        <strong><p class="mt-4 fs-3">Ognuno di noi è un appassaionato e non vediamo l'ora di condividere una birra con voi!</p></strong> 
        <a class="btn btn-primary btn-xl mt-3 mb-4" href="#about">Incontriamoci</a>
    </div>
</header>

<!-- About-->
<section class="content-section bg-light" id="about">
    <div class="container px-4 px-lg-5 text-center">
        <div class="row gx-4 gx-lg-5 justify-content-center">
            <div class="col-lg-10">
                <h2>Stylish Portfolio is the perfect theme for your next project!</h2>
                <p class="lead mb-5">
                    This theme features a flexible, UX friendly sidebar menu and stock photos from our friends at
                    <a href="https://unsplash.com/">Unsplash</a>
                    !
                </p>
                <a class="btn btn-dark btn-xl" href="#services">What We Offer</a>
            </div>
        </div>
    </div>
</section>

@endsection



