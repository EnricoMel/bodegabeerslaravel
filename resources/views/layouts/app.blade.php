<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <title>Bodega</title>
    <!-- {{ config('app.name', 'Laravel') }} -->
    {{-- <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script> --}}
        
    {{-- <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet"> --}}
            
    <!-- Favicon-->
    <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
    <!-- Font Awesome icons (free version)-->
    <script src="https://use.fontawesome.com/releases/v6.1.0/js/all.js" crossorigin="anonymous"></script>
    <!-- Simple line icons-->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.5.5/css/simple-line-icons.min.css" rel="stylesheet" />
    <!-- Google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css" />
    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="/css/styles.css" rel="stylesheet" />
            
    {{-- <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet"> --}}
</head>
<body id="page-top">
    <div id="app">
                    
       <!-- Navigation-->
       <a class="menu-toggle rounded" href="#"><i class="fas fa-bars"></i></a>
        <nav id="sidebar-wrapper">
           <ul class="sidebar-nav">                
                <li class="sidebar-brand"><a href="{{ route('index') }}">Home</a></li>
                <li class="sidebar-nav-item"><a href="{{ route('breweries') }}">Breweries</a></li>
                <li class="sidebar-nav-item"><a href="{{ route('about') }}">About</a></li>
                <li class="sidebar-nav-item"><a href="{{ route('team') }}">Team</a></li>
                            
                <!-- Authentication Links -->
                    @guest
                        @if (Route::has('login'))
                            <li class="sidebar-nav-item">
                                <a href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                        @endif
                            
                        @if (Route::has('register'))
                            <li class="sidebar-nav-item">
                                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                            </li>
                        @endif

                        @else
                            
                        <li class="sidebar-nav-item">
                            <a href="#"><em>Benvenuto {{ Auth::user()->name }} </em></a>
                        </li>
                            
                        <li class="sidebar-nav-item">
                           <a class="dropdown-item" href="{{ route('logout') }}"
                                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                {{ __('Logout') }}
                           </a>
                            
                            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                            @csrf
                            </form>
                        </li>        
                    @endguest                  
            </ul>
        </nav>
            <main>
                @yield('content')
            </main>
    </div>
            
    <!-- Footer-->
    <footer class="footer text-center">
        <div class="container px-4 px-lg-5">
            <ul class="list-inline mb-5">
                <li class="list-inline-item">
                    <a class="social-link rounded-circle text-white mr-3" href="#!"><i class="icon-social-facebook"></i></a>
                </li>
                <li class="list-inline-item">
                    <a class="social-link rounded-circle text-white mr-3" href="#!"><i class="icon-social-twitter"></i></a>
                </li>
                <li class="list-inline-item">
                    <a class="social-link rounded-circle text-white" href="#!"><i class="icon-social-github"></i></a>
                </li>
             </ul>
                <p class="text-muted small mb-0">Copyright &copy; Bodega!</p>
        </div>
    </footer>


    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top"><i class="fas fa-angle-up"></i></a>

    <!-- Bootstrap core JS-->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js"></script>

    {{-- Jquery cdn --}}
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    
    <!-- Core theme JS-->
    <script src="/js/scripts.js"></script>

    @stack('scripts')            <!-- qui verrà insetito uno script -->
    
</body>
</html>
        