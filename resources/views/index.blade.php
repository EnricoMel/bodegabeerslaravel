@extends('layouts.app')

@section('content') 

<style>
    
    .masthead {
        background: linear-gradient(90deg, rgba(220, 178, 78, 0.1) 0%, rgba(220, 178, 178, 0.1) 100%), url("/img/index_bg.jpg");
    }      
    
    .bg-trasparency {
        background: linear-gradient(90deg, rgba(255, 255, 255, 0.6) 0%, rgba(255, 255, 255, 0.6) 100%);
    }
    
    .segnala {
        background: linear-gradient(90deg, rgba(220, 178, 78, 0.6) 0%, rgba(220, 178, 178, 0.6) 100%), url("/img/segnala_bg.jpg");
    }  
    
</style>

<!-- Header-->
<header class="masthead d-flex align-items-center">
    <div class="container-fluid px-4 px-lg-5 text-center bg-trasparency">
        <h1 class="mb-1">BodegaBeers!</h1>
        <h3 class="mb-5"><em>Il portale per trovare la tua birra preferita in città</em></h3>

        {{-- Search --}}
        <form action="{{ route('search') }}" method="GET" class="mb-5">
            <input type="text" style="width: 500px;" placeholder="Search" name="q">
            <button class="btn btn-danger" type="submit">Ricerca</button>
        </form>
        
        <a class="btn btn-primary btn-xl mb-4" href="#about">Scopri di più</a>
        <a class="btn btn-primary btn-xl mb-4" href="{{ route('breweries') }}">Vai alle birrerie!</a>
        
    </div>
</header>
<!-- About-->
<section class="content-section bg-light" id="about">
    <div class="container px-4 px-lg-5 text-center">
        <div class="row gx-4 gx-lg-5 justify-content-center">
            <div class="col-lg-10">
                <h2>Grazie a Get a beer potrai trovare la tua birra preferita in ogni città</h2>
                <p class="lead mb-5">
                    Il nostro sistema di classificazione birrerie e ricerca ti stupirà!
                    <a href="{{ route('breweries')}}">Birrerie!</a>
                    !
                </p>
                <a class="btn btn-dark btn-xl" href="#segnala">Segnala la tua birreria preferita</a>
            </div>
        </div>
    </div>
</section>
<!-- Services-->
<section class="content-section bg-primary text-white text-center" id="services">
    <div class="container px-4 px-lg-5">
        <div class="content-section-heading">
            <h3 class="text-secondary mb-0">Funzionalità</h3>
            <h2 class="mb-5">Cosa potrai fare</h2>
        </div>
        <div class="row gx-4 gx-lg-5">
            <div class="col-lg-3 col-md-6 mb-5 mb-lg-0">
                <span class="service-icon rounded-circle mx-auto mb-3"><i class="icon-screen-smartphone"></i></span>
                <h4><strong>Birrerie</strong></h4>
                <p class="text-faded mb-0">Cercare una birreria</p>
            </div>
            <div class="col-lg-3 col-md-6 mb-5 mb-lg-0">
                <span class="service-icon rounded-circle mx-auto mb-3"><i class="icon-pencil"></i></span>
                <h4><strong>Commenti</strong></h4>
                <p class="text-faded mb-0">Commentare una birreria</p>
            </div>
            <div class="col-lg-3 col-md-6 mb-5 mb-md-0">
                <span class="service-icon rounded-circle mx-auto mb-3"><i class="icon-like"></i></span>
                <h4><strong>Favorite</strong></h4>
                <p class="text-faded mb-0">
                    Mantenere le birre preferite
                    <i class="fas fa-heart"></i>
                    
                </p>
            </div>
            <div class="col-lg-3 col-md-6">
                <span class="service-icon rounded-circle mx-auto mb-3"><i class="icon-mustache"></i></span>
                <h4><strong>Birre</strong></h4>
                <p class="text-faded mb-0">Cercare per tipo di birra</p>
            </div>
        </div>
    </div>
</section>

<section class="content-section bg-primary text-white masthead segnala d-flex" id="segnala">
    <div class="container-fluid text-center bg-trasparency">
        <div class="row">
            <div class="col-8 offset-2">
                <h2 class="mb-1 mt-2">Segnala la tua birreria preferita</h2>
                <h4 class="mb-5"><em>Sicuramente il gestore ti offrirà una birra!</em></h4>
                
                {{-- Messages validation --}}
                @if ($errors->any())
                <div class="alert alert-danger mt-3">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                {{-- End messages validation --}}

                <form method="POST" action="{{ route('breweries.notify')}}" enctype="multipart/form-data">
                    @csrf
                    <div class="row mb-2">
                        <div class="col">
                            <strong>Nome:</strong><br>
                            <input type="text" class="form-control" name="name" value="{{ old('name') }}">
                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col">
                            <strong>Immagine:</strong><br>
                            <input type="file" class="form-control" name="img">
                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col">
                            <strong>Descrizione:</strong><br>
                            <textarea id="" cols="30" rows="10" class="form-control" name="description">{{ old('description') }}</textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col mb-2">
                            <button type="submit" class="btn btn-primary btn-xl mt-2">Invia</button>
                        </div>
                    </div>
                    
                </form>
            </div>
        </div>
    </div>
</section>


@endsection



