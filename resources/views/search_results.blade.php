@extends('layouts.app')

@section('content')

<section class="content-section" id="portfollio">
    <div class="container">
        <div class="content-section-heading text-center mb-5">
            <h1 class="text-secondary mb-0"> Risultati ricerca per: {{ $q }}</h1>
        </div>
        
        <div class="row no-gutters">
            @foreach ($breweries as $brewery)
            <div class="col-lg-6">
                <a class="portfolio-item" href="{{ route('breweries.details', ['id' => $brewery->id]) }}">
                    <span class="caption">
                        <span class="caption-content">
                            <h2> {{ $brewery->name }}</h2>
                            <p class="mb-0"> {{ $brewery->description }}</p>
                            
                            <p>
                                @foreach ($brewery->beers as $beer)
                                <i>
                                    {{ $beer->name }}
                                </i>
                                @endforeach
                            </p>
                            
                            <p>Recensioni: {{ $brewery->comments->count() }}</p>
                            
                            @if ($brewery->visible == 0)
                            <form action="{{ route('brewery.approved', ['id' => $brewery->id] )}}" method="POST">
                                @csrf
                                <button type="submit" class="btn btn-primary mt-3">Rendila visibile!</button>
                            </form>
                            @endif      
                        </span>  
                    </span>
                    <img class="img-fluid" src="{{ Storage::url($brewery->img) }}" alt="{{ $brewery->name }}"/>
                </a>
            </div>
            @endforeach
        </div>
    </div>
</section>
@endsection

