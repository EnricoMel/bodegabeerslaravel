    
@extends('layouts.app')
    
@section('content')    
    
    <style>
        
        .masthead {
            background: linear-gradient(90deg, rgba(220, 178, 78, 0.1) 0%, rgba(220, 178, 178, 0.1) 100%), url("/img/about_bg.jpg");
        }      
        
        .bg-trasparency {
            background: linear-gradient(90deg, rgba(255, 255, 255, 0.6) 0%, rgba(255, 255, 255, 0.6) 100%);
        }
        
    </style>
    
    <!-- Header-->
    <header class="masthead d-flex align-items-center">
        <div class="container px-4 px-lg-5 text-center bg-trasparency">
            <h1 class="mb-1">Get a beer!</h1>
            <h3 class="mb-5"><em>Scoprite chi c'è dietro la birra che berrete stasera</em></h3>
            
            <h3 class="mb-3">Contattaci</h3>
            
            <form method="POST" action="{{ route('contacts.submit') }}">
                @csrf
                <div class="row mb-2">
                    <div class="col">
                        <input type="text" class="form-control" placeholder="Nome" name="name">
                    </div>
                    <div class="col">
                        <input type="text" class="form-control" placeholder="Cognome" name="surname">
                    </div>
                </div>
                
                <div class="row mb-2">
                    <div class="col">
                        <input type="text" class="form-control" placeholder="Telefono" name="phone">
                    </div>
                    <div class="col">
                        <input type="email" class="form-control" placeholder="Email" name="email">
                    </div>
                </div>
                
                <div class="row mb-2">
                    <div class="col">
                        <strong>Messaggio:</strong><br>
                        <textarea class="form-control" name="message" id="" cols="30" rows="10"></textarea>
                    </div>
                </div>
                
                <div class="row mb-2">
                    <div class="col">
                        <button type="submit" class="btn btn-primary my-2">Invia</button>
                    </div>
                </div>
            </form>
        </div>
    </header>
    
    <!-- About-->
    <section class="content-section bg-light" id="about">
        <div class="container px-4 px-lg-5 text-center">
            <div class="row gx-4 gx-lg-5 justify-content-center">
                <div class="col-lg-10">
                    <h2>Stylish Portfolio is the perfect theme for your next project!</h2>
                    <p class="lead mb-5">
                        This theme features a flexible, UX friendly sidebar menu and stock photos from our friends at
                        <a href="https://unsplash.com/">Unsplash</a>
                        !
                    </p>
                    <a class="btn btn-dark btn-xl" href="#services">What We Offer</a>
                </div>
            </div>
        </div>
    </section>
    
@endsection
    
    
    
    
    
    