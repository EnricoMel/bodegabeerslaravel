@extends('layouts.app')

@section('content') 

<style>
    
    .masthead {
        background: linear-gradient(90deg, rgba(220, 178, 78, 0.1) 0%, rgba(220, 178, 178, 0.1) 100%), url("{{ Storage::url($brewery->img) }}");
    }      
    
    .bg-trasparency {
        background: linear-gradient(90deg, rgba(255, 255, 255, 0.6) 0%, rgba(255, 255, 255, 0.6) 100%);
    }
    
</style>


@if (session('message'))
<div class="alert alert-success text-center">
    {{ session('message') }}
</div>
@endif

<!-- Header-->
<header class="masthead d-flex">
    <div class="container text-center my-auto bg-trasparency">
        <h1 class="mb-1">{{ $brewery->name }}</h1>
        <h3 class="mb-4">
            <br>     
            <em>{{ $brewery->description }}</em>
            <br>
            <a href="{{ route('breweries') }}" class="btn btn-primary btn-xl mt-5">Torna alle birrerie </a>
        </h3>  

        <h4> Birre disponibili</h4>
        @foreach ($brewery->beers as $beer)
        <strong>
            {{ $beer->name }} <br>

        </strong>
        @endforeach

    </div>
</header>

{{-- Edit breweries for admin --}}
@auth
@if (Auth::user()->isAdmin)                     {{-- io che sono un utente autenticato e amministratore accedo all'edit birreria --}}

@if ($errors->any())
<div class="alert alert-danger text-center mt-4"> 
    
    @foreach ($errors->all() as $error)
    {{ $error }}
    @endforeach
    
</div>
@endif

<div class="container my-5 border border-3 rounded">
    <h1 class="text-center mt-2">Area amministratori</h1>
    <h2 class="text-center mb-5">Pannello modifica birreria</h2>
    <form action="{{ route('breweries.update', ['id' => $brewery->id]) }}" method="POST" enctype="multipart/form-data">
        @csrf
        
        <div class="form-group row mb-3 align-items-center">
            <label for="name" class="col-sm-2 col-form-label">Nome</label>
            <div class="col-sm-10">
                <input type="text"  id="name" class="form-control" value="{{ old('name', $brewery->name) }}" name="name">
            </div>
        </div>
        
        <div class="form-group row mb-3 align-items-center">
            <label for="img" class="col-sm-2 col-form-label">Immagine</label>
            <div class="col-sm-10">
                <input type="file" id="img" class="form-control" name="img">
            </div>
        </div>
        
        <div class="form-group row mb-3 align-items-center">
            <label for="description" class="col-sm-2 col-form-label">Descrizione</label>
            <div class="col-sm-10">
                <textarea name="description" id="description" class="form-control" name="description">{{ old('description', $brewery->description) }}</textarea>
            </div>
        </div>
        
        <div class="form-group row mb-3 align-items-center">
            <label for="lat" class="col-sm-2 col-form-label">Latitudine</label>
            <div class="col-sm-4">
                <input type="text" id="lat" class="form-control" value="{{ old('lat', $brewery->lat) }}" name="lat">
            </div>
            
            <label for="lon" class="col-sm-2 col-form-label">Longitudine</label>
            <div class="col-sm-4">
                <input type="text" id="lon" class="form-control" value="{{ old('lon', $brewery->lon) }}" name="lon">
            </div>
        </div>
        
        <div class="form-group row mb-5 align-items-center">
            <label for="img" class="col-sm-2 col-form-label">Salva modifiche</label>
            <div class="col-sm-10">
                <button type="submit" class="btn btn-primary">Salva modifiche</button>
            </div>
        </div>
    </form>
    
    {{-- Delete brewery --}}
    <div class="row">
        <div class="col-md-12">
            <form action="{{ route('breweries.delete', ['id' => $brewery->id] )}}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('DELETE')
                <button type="submit" class="btn btn-danger mb-2">Elimina la birreria</button>
            </form>
        </div>
    </div>
</div>

<br>

{{-- Associa birre  --}}
<div class="container">
    <h1>Associa birre</h1>
    
    <div class="row">
        <div class="col-sm-4">
            <form action="{{ route('breweries.beers.sync', ['id' => $brewery->id ]) }}" method="POST">
                @csrf
                
                <ul id="beers">
                    @foreach ($brewery->beers as $beer)
                    <li>
                        {{ $beer->id }} - {{ $beer->name }}
                        <input type="hidden" value="{{ $beer->id }}" name="beer_ids[]">
                        <button type="button" class="btn btn-danger btn-xs remove_beer">Disassocia</button>
                    </li>
                    @endforeach
                </ul>
                
                <button type="submit" class="btn btn-success btn-xl">Associa</button>
                
            </form>
        </div>
        
        <div class="col-sm-8">
            RICERCA&nbsp;&nbsp<input type="text" id="beersearch">
            <br>
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Nome</th>
                        <th scope="col">Descrizione</th>
                        <th scope="col">ABV</th>
                        <th scope="col">IBU</th>
                    </tr>
                </thead>
                
                @foreach ($beers as $beer)
                <tbody>
                    <tr class="beer_row" index="{{ strtolower($beer->name) }}_{{ strtolower($beer->description) }}" style="display:none;">
                        <th scope="row">{{ $beer->id }}</th>
                        <td>{{ $beer->name }}</td>
                        <td>{{ $beer->description }}</td>
                        <td>{{ $beer->abv }}</td>
                        <td>{{ $beer->ibu }}</td>
                        <td><button class="btn btn-success add_beer btn-sm" beer-id="{{ $beer->id }}" beer-name="{{ $beer->name }}">Aggiungi</button></td>
                    </tr>
                </tbody>
                @endforeach
            </table>
        </div>
    </div>
</div>



@endif  
@endauth

{{-- comments --}}

@auth
@if ($errors->any())
<div class="alert alert-danger mt-3 text-danger">
    
    @foreach ($errors->all() as $error)
    {{ $error }}
    @endforeach
    
</div>
@endif
<div class="container mt-5">
    <h2>Inserisci una recensione</h2>
    <form action="{{ route('breweries.comments.add', ['id' => $brewery->id]) }}" method="POST">
        @csrf
        
        <div class="form-group row">
            <label for="comment" class="cols-sm-2 col-form-label">Commento</label>
            <div class="col-sm-10">
                <textarea id="" class="form-control" name="comment"> {{ old('comment') }}</textarea>
            </div>
        </div>
        
        <div class="form-group row">
            <label for="comment" class="cols-sm-2 col-form-label"></label>
            <div class="col-sm-10">
                <button type="submit" class="btn btn-primary btn-xl">Pubblica commento</button>
            </div>
        </div>
    </form>
</div>
@endauth

{{-- Visualizzazione recensioni --}}
<section class="content-section" id="comments">
    <div class="container">
        <div class="content-section-heading text-center">
            <h3 class="text-secondary mb-0">Commenti</h3>
            <h2 class="mb-5">Scopri cosa pensano gli utenti della birreria <strong>{{ $brewery->name }}</strong></h2>
        </div>
        <div class="row no-gutters">
            @foreach ($brewery->comments as $comment)
            <div class="col-log-6">
                <div class="p-3 mb-2 bg-primary text-white">
                    <span class="caption">
                        <span class="caption-content">
                            <h2>{{ $comment->user->name }}</h2>
                            <p class="mb-0">{{ $comment->comment }}</p>
                        </span>
                    </span>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>

<!-- Map-->
<section class="map" id="contact">
    {{-- <iframe src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Twitter,+Inc.,+Market+Street,+San+Francisco,+CA&amp;aq=0&amp;oq=twitter&amp;sll=28.659344,-81.187888&amp;sspn=0.128789,0.264187&amp;ie=UTF8&amp;hq=Twitter,+Inc.,+Market+Street,+San+Francisco,+CA&amp;t=m&amp;z=15&amp;iwloc=A&amp;output=embed"></iframe> --}}
    {{-- <br /> --}}
    {{-- <small><a href="https://maps.google.com/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=Twitter,+Inc.,+Market+Street,+San+Francisco,+CA&amp;aq=0&amp;oq=twitter&amp;sll=28.659344,-81.187888&amp;sspn=0.128789,0.264187&amp;ie=UTF8&amp;hq=Twitter,+Inc.,+Market+Street,+San+Francisco,+CA&amp;t=m&amp;z=15&amp;iwloc=A"></a></small> --}}
    
    <div id="brewery-map" class="img-fluid w-100" style="height: 500px"
    lat="{{ $brewery->lat }}"
    lon="{{ $brewery->lon }}"
    name="{{ $brewery->name }}"
    description="{{ $brewery->description }}"
    >
    
    
</div>

</section>

@push('scripts')

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCmRidWXFVKBM2Oqu3M2HdoROKh119nZyY"></script>  <!-- c'è un problema con questa chiamata, account di fatturazione google --> 

<script>
    
    $(function() {
        
        let brewery_map = $("#brewery-map");
        
        if (! brewery_map) {         // se non c'è la mappa non fare nulla, il div brewery_map
        return;
    }      
    
    let lan = brewery_map.attr('lan');
    let lon = brewery_map.attr('lon');
    
    if (! lan || ! lon) {       // se non c'è l'attributo lan o lon non fare niente
    return;
}

// Sapendo che c'è l'elemento e che ha quegli attributi posso procedere

let position = new google.maps.LatLng(lat, lon);

let map = new google.maps.Map(
document.getElementById('brewery-map'),
{center: position, zoom: 18}
);

let marker = new google.maps.Marker({
    position: position,
    icon: '/img/beer_here.png',
    map:map
});

let name = brewery_map.attr('name');
let description = brewery_map.attr('description');
let infowindow = new google.mapsInfoWindow({
    content: '<strong>' + name + '</strong><br><i>' + description + '<i>'
    });
    
    marker.addListener('click', function() {
        infowindow.open(map, marker);
    });
    
});  

</script>

<script>
    
    
    
    $(function(){
        
        // Ricerca birre
        
        $('#beersearch').on('keyup', function() {
            let value = $(this).val();
            
            // solo dopo 3 caratteri, fai scattare la ricerca
            if(value.length <= 2) {
                // nascondi tutto
                $(".beer_row").hide();
                return;
            }
            
            // prendi il testo tutto in minuscolo
            value = value.toLowerCase();
            
            // nascondi tutto
            $(".beer_row").hide();
            
            // rendi visibile solo ciò che contiene il testo in value
            $('[index*="' + value + '"]').show();
            
        });  
        
        
        function attachRemoveBeer() {
            $(".remove_beer").on('click', function() {
                $(this).parent().remove();
            })
        }
        
        $(".add_beer").on('click', function() {
            let beer_id = $(this).attr('beer-id');
            let beer_name = $(this).attr('beer-name');
            
            let beers = $('#beers');
            
            let html = `
            
            <li>
                ${beer_id} - ${beer_name}
                <input type="hidden" name="beer_ids[]" value="${beer_id}">
                <button type="button" class="btn btn-danger btn-xs remove_beer">Disassocia</button>
            </li>
            
            `;
            
            beers.append(html);
            attachRemoveBeer();
        });

        attachRemoveBeer();
         
    });
    
</script>

@endpush

@endsection


