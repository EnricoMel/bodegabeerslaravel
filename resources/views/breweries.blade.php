@extends('layouts.app')

@section('content') 

<style>
    
    .masthead {
        background: linear-gradient(90deg, rgba(220, 178, 78, 0.1) 0%, rgba(220, 178, 178, 0.1) 100%), url("/img/breweries_bg.jpg");
    }      
    
    .bg-trasparency {
        background: linear-gradient(90deg, rgba(255, 255, 255, 0.6) 0%, rgba(255, 255, 255, 0.6) 100%);
    }
    
</style>

@if (session('message'))
<div class="alert alert-success text-center">
    {{ session('message') }}
</div>
@endif

<!-- Header-->
<header class="masthead d-flex align-items-center">
    <div class="container px-4 px-lg-5 text-center bg-trasparency">
        <h1 class="mb-1">Get a beer!</h1>
        <h3 class="mb-5"><em>La birreria migliore sempre vicino a te</em></h3>
        
        <h1>Scegline una ma provale tutte!</h1>
        <a class="btn btn-primary btn-xl mt-3 mb-4 mx-2" href="{{ route('index')}}">Back</a>
        
    </div>
</header>

<!-- Portfolio-->
<section class="content-section" id="portfolio">
    <div class="container px-4 px-lg-5">
        <div class="content-section-heading text-center">
            <h3 class="text-secondary mb-0">Scopri le nostre</h3>
            <h2 class="mb-5">Birrerie</h2>
        </div>
        <div class="row gx-0">
            @foreach ($breweries as $brewery)
            
            <div class="col-md-6">
                <a class="portfolio-item" href="{{ route('breweries.details', ['id' => $brewery->id] )}}">
                    <div class="caption">
                        <div class="caption-content">
                            <div class="h1 text-uppercase">{{ $brewery->name }}</div>
                            <p class="mb-0">{{ $brewery->description }}</p>
                            
                            <p>
                                <h5>Birre disponibili</h5>
                                @foreach ($brewery->beers as $beer)
                                <i>
                                    {{ $beer->name }} -
                                </i>
                                @endforeach
                            </p>

                            <p>Recensioni: {{ $brewery->comments->count() }}</p>
                            
                            @if ($brewery->visible == 0)
                            <form action="{{ route('brewery.approved', ['id' => $brewery->id] )}}" method="POST">
                                @csrf
                                <button type="submit" class="btn btn-primary mt-3">Rendila visibile!</button>
                            </form>
                            @endif
                            
                        </div>
                    </div>
                    <img class="img-fluid" src="{{ Storage::url($brewery->img) }}" alt="{{ $brewery->name }}"/>
                </a>
            </div>
            
            @endforeach
            
        </div>
    </div>
</section>

<!-- Map-->
<section class="map" id="contact">
    {{-- <iframe src="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Twitter,+Inc.,+Market+Street,+San+Francisco,+CA&amp;aq=0&amp;oq=twitter&amp;sll=28.659344,-81.187888&amp;sspn=0.128789,0.264187&amp;ie=UTF8&amp;hq=Twitter,+Inc.,+Market+Street,+San+Francisco,+CA&amp;t=m&amp;z=15&amp;iwloc=A&amp;output=embed"></iframe>
    <br />
    <small><a href="https://maps.google.com/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=Twitter,+Inc.,+Market+Street,+San+Francisco,+CA&amp;aq=0&amp;oq=twitter&amp;sll=28.659344,-81.187888&amp;sspn=0.128789,0.264187&amp;ie=UTF8&amp;hq=Twitter,+Inc.,+Market+Street,+San+Francisco,+CA&amp;t=m&amp;z=15&amp;iwloc=A"></a></small> --}}
    
    
    <div id="all-breweries-map" class="img-fluid w-100" style="height: 500px" breweries=" {{ $breweries }}">
        
    </div>
    
    
</section>

@push('scripts')

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCmRidWXFVKBM2Oqu3M2HdoROKh119nZyY"></script>

<script>
    
    $(function() {
        let brewery_map = $('#all-breweries-map');
        
        if (! brewery_map) {
            return;
        }
        
        let breweries = JSON.parse(brewery_map.attr('breweries'));
        
        if (! breweries && breweries.lenght <= 0) {
            return;
        }
        
        function hasLatLon(brewery) {
            return brewery.lat && brewery.lat !=0 && brewery.lat && brewery.lat !=0;
        }
        
        breweries = breweries.filter(hasLatLon);
        
        // costruiamo un box che includa tutte le birrerie
        
        var bounds = new google.maps.LatLngBounds();
        breweries.forEach(brewery => {
            let position = new google.maps.LatLng(brewery.lat, brewery.lon);
            bounds.extend(position);
        });
        
        // trova il centro
        let center_position = bounds.getCenter();
        
        // creo la mappa
        let map = new google.maps.Map(
        document.getElementById('all-breweries-map'),
        {center: center_position, zoom: 17}
        );
        
        breweries.forEach(brewery => { 
            let position = new google.maps.LatLng(brewery.lat, brewery.lon);
            
            ler marker = new google.maps.Marker({
                position: position,
                icon: '/img/beer_here.png',
                map: map,
            });
            
            let infowindow = new google.maps.InfoWindow({
                content: '<strong>' + brewery.name + '</strong><br><i>' + brewery.description + '<i>'
                });
                
                marker.addListener('click', function(){
                    infowindow.open(map, marker);
                });
                
            })
        });
        
    </script>
    @endpush
    @endsection
    
    
    