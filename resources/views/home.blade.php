@extends('layouts.app')

@section('content')
<div class="container mt-5">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>
                
                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                    @endif
                    
                    <div class="row mb-2 justify-content-center">
                        <div class="col text-center">
                            <h2>{{ __('You are logged in!') }}</h2>
                        </div>
                    </div>
                    
                    <div class="row mb-2 justify-content-center">
                        <div class="col text-center">
                            <a class="btn btn-primary mt-3 mb-4 mx-2" href="{{ route('index')}}">Back</a>              
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection