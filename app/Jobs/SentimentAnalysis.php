<?php

namespace App\Jobs;

use App\Models\Comment;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SentimentAnalysis implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    
    /**
    * Create a new job instance.
    *
    * @return void
    */
    
    
    private  $comment_id;
    
    
    public function __construct($comment_id)
    {
        $this->comment_id = $comment_id;
    }
    
    /**
    * Execute the job.
    *
    * @return void
    */
    public function handle()
    {
        $comment = Comment::find($this->comment_id);          // cattturiamo il commento
        $commentText = $comment->comment;                     // catturiamo il testo presente nel commento
        
        $cloud = new ServiceBuilder([                                              // istanziamo un nuovo service builder
            'keyFilePath' => base_path(‘google_credentials.json’),
            'projectId' => ‘getabeer’
        ]);
        
        $language = $cloud->language();
        $annotation = $language->analyzeSentiment($commentText);
        $sentiment = $annotation->sentiment();
        $sentiment_score = $sentiment['score'];

        if ($sentiment_score < 0) {
            // Manda email

            $bag = [
                'comment' => $commentText,
                'pub' => $comment->brewery->name,
                'score' =>$sentiment_score
            ];

            $contactMail = new NegativeCommentReceived($bag);
            $emailAdmin = 'enricomlz@hotmail.it';
            Mail::to($emailAdmin)->send($contactMail);
        }
    }
}
