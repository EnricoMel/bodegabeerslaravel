<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CommentsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'comment'=> 'required|min:15|max:8000',
        ];
    }

    public function messages()
    {
        return [
            
            'comment.required' => 'È obbligatorio inserire un commento',
            'comment.min' => 'Il commento deve essere di almeno 50 caratteri',
            'comment.max' => 'Il commento non può superare gli 8000 caratteri',
            
            
        ];
    }
}
