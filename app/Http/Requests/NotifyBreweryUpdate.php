<?php

namespace App\Http\Requests;

use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Http\FormRequest;

class NotifyBreweryUpdate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // logica applicativa

        $user = Auth::user();
        return ($user && $user->isAdmin);       // ritorna un valore boolenao

    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name'=> 'required|max:160',
            'description'=> 'required|max:8000',
            'lat' => ['required', 'regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'],
            'lon' => ['required', 'regex:/^[-]?((((1[0-7][0-9])|([[0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/']
        ];
    }

    public function messages()
    {
        return [
            
            'name.required' => 'È obbligatorio inserire un titolo',
            'name.max' => 'Il nome della birreria non può superare i 160 caratteri',
            'description.required' => 'È obbligatorio inserire una descrizione',
            'description.max' => 'La descrizione non può superare gli 8000 caratteri',
          
            
        ];
    }
}
