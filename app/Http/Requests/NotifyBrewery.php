<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NotifyBrewery extends FormRequest
{
    /**
    * Determine if the user is authorized to make this request.
    *
    * @return bool
    */
    public function authorize()
    {
        return true;
    }
    
    /**
    * Get the validation rules that apply to the request.
    *
    * @return array<string, mixed>
    */
    public function rules()
    {
        return [
            'name'=> 'required|max:160',
            'description'=> 'required|max:8000',
            'img'=> 'required|image'
        ];
    }
    
    public function messages()
    {
        return [
            
            'name.required' => 'È obbligatorio inserire un titolo',
            'name.max' => 'Il nome della birreria non può superare i 160 caratteri',
            'description.required' => 'È obbligatorio inserire una descrizione',
            'description.max' => 'La descrizione non può superare gli 8000 caratteri',
            'img.required' => 'È obbligatorio inserire un\'immagine',
            'img.image' => 'Il file inserito non è un\'immagine'
            
        ];
    }

    protected function getRedirectUrl() {
        $url = $this->redirector->getUrlGenerator();
        return $url->previous() . '#segnala';
    }
}
