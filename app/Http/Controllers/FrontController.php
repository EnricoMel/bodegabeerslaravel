<?php

namespace App\Http\Controllers;

use App\Models\Brewery;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class FrontController extends Controller
{
    
    public function index() {
        
        // $myTime = Carbon::now();
        // $time = $myTime->toDateTimeString(); ['time' => $time]) questo il dato che passavamo alla vista
        return view('index');
    }
    
    
    public function breweries() {
        // $breweries = [
            
            //     ['name' => 'The temple', 'description' => 'Aperti da più di cento anni, la nostra storia da il gusto alle nostre birre', 'img' => '/img/pub2_bg.jpg' ],
            //     ['name' => 'Joyce', 'description' => 'Vieni a divertirti nel nostro pub', 'img' => '/img/pub2_bg.jpg'],
            //     ['name' => 'St.Louis', 'description' => 'Un percorso storico tra le birre del mondo! Ti aspettiamo', 'img' => '/img/pub3_bg.jpg'],
            //     ['name' => 'La quarta media', 'description' => 'Bevi il meglio, bevi di qualità', 'img' => '/img/pub4_bg.jpg'],
            //     ['name' => 'A beer please', 'description' => 'Ogni birra è un mondo da scoprire da noi!', 'img' => '/img/pub5_bg.jpg'],
            //     ['name' => 'The gold river', 'description' => 'Fiumi biondi, rossi e ambrati ti sorprenderanno', 'img' => '/img/pub6_bg.jpg']
            
            // ];

         $user = Auth::user();
            if ($user && $user->isAdmin) {
                $breweries = Brewery::with(['beers', 'comments'])->orderBy('id', 'desc')->get();
            } else {
                $breweries = Brewery::with(['beers', 'comments'])->where('visible', '=', true)->orderBy('id', 'desc')->get();
            }
           
        return view('breweries', compact('breweries'));
    }
        
    public function about() {
            return view('about');
        }
        
    public function team() {
            $members = [
                
                ['name'=>'Giancarlo', 'role' => 'Senior developer', 'img' => '/img/face.jpg'],
                ['name'=>'Enrico', 'role' => 'Developer', 'img' => '/img/face.jpg'] ,
                ['name'=>'Luca', 'role' => 'Developer mobile', 'img' =>'/img/face.jpg'],
                ['name'=>'Nico', 'role' => 'Graphics', 'img' =>'/img/face.jpg'],
                ['name'=>'Jepson', 'role' => 'Bo', 'img' =>'/img/face.jpg']
                
                
            ];
            
            return view('team', compact('members'));
    }

    // Search

    public function search(Request $req)
    {
        $q = $req->input('q');
        $breweries = Brewery::search($q)->where('visible', true)->query(function($builder) {
            $builder->with(['beers', 'comments']);
        })->get();

        return view('search_results', compact('q', 'breweries'));
    }
        
}
    
    
    
    