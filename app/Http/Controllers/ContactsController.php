<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\ContactReceived;
use Illuminate\Support\Facades\Mail;

class ContactsController extends Controller
{
    public function submit(Request $req) {
       
        $name = $req->input('name');
        $surname = $req->surname;         // $re è un oggetto e quindi usare la sintassi dell'oggetto, con gli input è più didattico e formativo
        $phone = $req->input('phone');
        $email = $req->input('email');
        $message = $req->input('message');


        // Invio email
        $emailAdmin = 'enricomlz@hotmail.it';
        $bag = compact('name', 'surname', 'phone', 'email', 'message');
        $contactMail = new ContactReceived($bag);
        Mail::to($emailAdmin)->send($contactMail);

        return redirect(route('contacts.thankyou'));

    }
    
    public function thankyou() {
        return view('contacts.thankyou');
    }
    
    
}
