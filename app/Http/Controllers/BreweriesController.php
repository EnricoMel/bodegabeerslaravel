<?php

namespace App\Http\Controllers;

use App\Models\Beer;
use App\Models\Brewery;
use App\Models\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\NotifyBrewery;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\CommentsRequest;
use Illuminate\Support\Facades\Schema;
use App\Http\Requests\NotifyBreweryUpdate;
use Whoops\Run;

class BreweriesController extends Controller
{
    public function notify(NotifyBrewery $req) {
        
        $name = $req->input('name');
        $description = $req->description;
        $img = $req->file('img')->store('/public/img');

        // Esploriamo alcune modalità di inserimento, di scrittura del database

        // DB::insert('insert into breweries(name, description, img, created_at, updated_at) 
        // values (?, ?, ?, ?, ?)', 
        // [
        //     $name, $description, 'img/pub3_bg.jpg', \Carbon\Carbon::now(), \Carbon\Carbon::now()  
        // ]);

        // DB::table('breweries')->insert([

        //     'name' => $name,
        //     'description' => $description,
        //     'img' => 'img/pub3_bg.jpg',
        //     'created_at' => \Carbon\Carbon::now(),
        //     'updated_at' => \Carbon\Carbon::now()
            
        // ]);

        // Mass assignment
        
        // $img ='img/pub3_bg.jpg';

        // Brewery::create([
        //     'name' => $name,
        //     'description' => $description,
        //     'img' => $img
        // ]);

        Brewery::create(compact('name', 'description', 'img'));

        // $breweryNotified = new Brewery();
        // $breweryNotified->name = $name;
        // $breweryNotified->description = $description;
        // $breweryNotified->img = '/img/pub3_bg.jpg';

        // $breweryNotified->save();

        return redirect(route('breweries.thankyou'));

    }

    public function thankyou() {
        return view('breweries.thankyou');
    }

    public function breweryApproved($id)
    {
        
        $user = Auth::user();
        
        if ($user && $user->isAdmin) {
            $brewery = Brewery::find($id);
            $brewery->visible = true;
            $brewery->save();

            return redirect(route('breweries'))->with('message', 'Birreria pubblicata');
        }

    }

    public function details($id)
    {
        $user = Auth::user();

        if ($user && $user->isAdmin) {
            $brewery = Brewery::with(['beers', 'comments'])->find($id);
        } else {

            $brewery = Brewery::with(['beers', 'comments'])->where('visible', true)->where('id', $id)->first();
        }

        if($brewery == null){
            return "Birreria non trovata";
        }

        $beers = Beer::all();

        return view('breweries.details', compact('brewery', 'beers'));
    }

    public function update(NotifyBreweryUpdate $req, $id)
    {
        $brewery = Brewery::find($id);                // leggiamo la birreria che ci arriva
       
        $name = $req->input('name');                  // qui possiamo scrivere diversamente il codice, in questo caso è solo per allenamento come l'abbiamo scritto.
        $description = $req->description;             // possiamo cio+ fare un inline. Le variabili di supporto ci servivano solo per didattica e allenamento. Vedi sotto.
        $img = $req->file('img');
        $lat = $req->input('lat');
        $lon = $req->lon;

        // $brewery->name = $req->input('name');      // questa è la modifica al codice sopra
        //$brewery->description = $req->description;

        if ($img != null) {
            $img = $img->store('public/img');            // se c'è un'immagine la memorizzo su e aggiorno il campo
            $brewery->img = $img;
        } 

        $brewery->name = $name;
        $brewery->description = $description;
        $brewery->lat = $lat;
        $brewery->lon = $lon;

        $brewery->save();

        return redirect(route('breweries.details', ['id' => $brewery->id]));
    }

    public function delete($id)
    {
        $user = Auth::user();
        if ($user && $user->isAdmin) {
             Brewery::destroy($id);           
        }

        return redirect(route('breweries'));

    }

    public function addComment(CommentsRequest $req, $id)
    {

       $user = Auth::user(); 
       $brewery = Brewery::find($id);              // recuperiamo la birreria tramite l'id

           $brewery->comments()->create([        // traversiamo la relazione per poter recuperare i commenti relazionati e con la create inseriamo il commento nel database
           'comment'=>$req->input('comment'),
           'user_id' => $user->id
       ]);

      return redirect(route('breweries.details', ['id'=>$id] ))->with('message', 'Recensione inserita correttamente');
    }

    public function beersSync(Request $req, $id)
    {
        $beer_ids = $req->input('beer_ids');
        
        $brewery = Brewery::find($id);
        $brewery->beers()->sync($beer_ids);

        return redirect(route('breweries.details', ['id' => $id] ));
    }

}