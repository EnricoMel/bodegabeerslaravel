<?php

namespace App\Models;


use App\Models\Beer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Laravel\Scout\Searchable;

class Brewery extends Model
{
    use HasFactory;
    use Searchable;
    
    protected $fillable = ['name', 'description', 'img'];
    
    public function toSearchableArray()
    {
        $birre = $this->beers->pluck('name')->join(', ');

        $array = [
            
            'id' => $this->id,                               // obbligatorio inserire l'id
            'name' => $this->name,
            'description' => $this->description,
            'altro' => 'birrerie birra',
            'birre' => $birre
        ];
        
        return $array;
    }
    
    public function comments()
    {
        return $this->hasMany(\App\Models\Comment::class);
    }
    
    public function beers()
    {
        return $this->belongsToMany(Beer::class);
    }
}
