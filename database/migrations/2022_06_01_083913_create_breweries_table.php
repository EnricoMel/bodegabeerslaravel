<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    
    public function up()
    {
        Schema::create('breweries', function (Blueprint $table) {
            $table->id();
            $table->string('name', 160);
            $table->text('description');
            $table->string('img');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('breweries');
    }

};
